﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions.Must;

namespace UnityUtil
{
    public class PostprocessTextures : AssetPostprocessor
    {
        public static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets,
                string[] movedAssets, string[] movedFromAssetPaths)
        {
            Regex regex = new Regex(@"\.(gif|jpg|jpeg|tiff|png)$");

            // Check if each asset has an image extension and if so make sure it's readable and transparent
            var imagePaths = from a in importedAssets where regex.Match(a).Success select a;
            foreach (var imagePath in imagePaths)
            {
                Debug.Log(string.Format("Postprocessing texture {0}", imagePath));
                var importer = GetTextureImporter(imagePath);
                importer.textureType = TextureImporterType.Advanced;
                if (!importer.isReadable || !importer.alphaIsTransparency)
                {
                    importer.isReadable = true;
                    importer.alphaIsTransparency = true;
                    ReimportAsset(imagePath);
                }
            }
        }

        public static TextureImporter GetTextureImporter(string texturePath)
        {
            var importer = AssetImporter.GetAtPath(texturePath) as TextureImporter;
            if (importer == null)
            {
                throw new Exception(string.Format("Trouble getting texture importer at {0}", texturePath));
            }
            return importer;
        }

        public static void ReimportAsset(string assetPath)
        {
            AssetDatabase.ImportAsset(assetPath);
            AssetDatabase.Refresh();
        }
    }
}