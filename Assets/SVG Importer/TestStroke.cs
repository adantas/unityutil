﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using SVGImporter;
using SVGImporter.Utils;
using SVGImporter.Rendering;

[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshFilter))]
[ExecuteInEditMode]
public class TestStroke : MonoBehaviour {

    public Color color;
    public float strokeWidth = 1f;
    public float strokeOffset = 0f;
    public ClosePathRule closed;

    List<Vector2> points;
	void Update()
    {
        MeshFilter mf = gameObject.GetComponent<MeshFilter>();

        Transform[] children = gameObject.GetComponentsInChildren<Transform>();
        if(points == null)
        {
            points = new List<Vector2>();
        } else {
            points.Clear();
        }

        Matrix4x4 worldToLocal = transform.worldToLocalMatrix;
        for(int i = 1; i < children.Length; i++)
        {
            points.Add(worldToLocal.MultiplyPoint(children[i].transform.position));
        }

        if(points.Count > 1)
        {
            mf.sharedMesh = SVGLineUtils.StrokeMesh(SVGSimplePath.GetSegments(points), strokeWidth, color, StrokeLineJoin.bevel, StrokeLineCap.butt, 4, null, 0, ClosePathRule.ALWAYS);
        }
    }

    void OnDrawGizmos()
    {
        if(points != null && points.Count > 1)
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Vector3 currentPoint, lastPoint = points[0];
            for(int i = 1; i < points.Count; i++)
            {
                currentPoint = points[i];
                Gizmos.DrawLine(lastPoint, currentPoint);
                lastPoint = currentPoint;
            }
        }
    }

}
