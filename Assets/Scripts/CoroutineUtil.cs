﻿using UnityEngine;
using System;
using System.Collections;

namespace UnityUtil
{
    public static class CoroutineUtil
    {

        public static IEnumerator WaitAndDo( Action action, float delay )
        {
            yield return new WaitForSeconds( delay );
            action( );
        }

        public static IEnumerator Wait( float time )
        {
            yield return new WaitForSeconds( time );
        }

        public static IEnumerator Do( Action action )
        {
            action( );
            yield return 0;
        }
    }
}