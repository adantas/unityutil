﻿using System.Collections.Generic;
using UnityEngine;

using SVGImporter.Utils;
using SVGImporter.Rendering;

namespace UnityUtil
{
    /// <summary>
    /// Uses SVGLineUtils. StrokeMesh to draw an Outline. Uses SVGLineUtils.StrokeMesh 
    /// to draw an Outline based on the SVGRenderer's shape. 
    /// </summary>
    [ExecuteInEditMode]
    public class OutlineSVG : MonoBehaviour
    {
        private List<Vector2> points;
        private GameObject outlineGO;

        public void OnEnable()
        {
            if( outlineGO != null ) // Only add outline if there isn't already an outline
            {
                return;
            }

            outlineGO = new GameObject( "Outline" );
            outlineGO.transform.parent = transform;
            outlineGO.transform.localPosition = Vector3.zero;

            var shapeMeshFilter = gameObject.GetComponent<MeshFilter>();
            var shapeMeshRenderer = gameObject.GetComponent<MeshRenderer>();

            var vertices = shapeMeshFilter.sharedMesh.vertices;
            if( points == null )
            {
                points = new List<Vector2>();
            }
            else
            {
                points.Clear();
            }

            for (var i = 0; i < vertices.Length; i++)
            {
                points.Add(vertices[i]);

                // TODO: remove this, for debug/visualization purposes
                // TODO: fix vertex ordering
                var vertexGO = new GameObject("debug-vertex-" + i);
                vertexGO.transform.parent = outlineGO.transform;
                vertexGO.transform.localPosition = vertices[i];
            }

            if( points.Count > 1 )
            {
                outlineGO.AddComponent<MeshRenderer>().sharedMaterials = shapeMeshRenderer.sharedMaterials;

                // TODO: remove these magic numbers, make them accessible in the editor
                outlineGO.AddComponent<MeshFilter>().sharedMesh =
                    SVGLineUtils.StrokeMesh(
                        SVGSimplePath.GetSegments( points ),
                        0.1f,
                        Color.black,
                        StrokeLineJoin.bevel,
                        StrokeLineCap.butt,
                        4,
                        null,
                        0,
                        ClosePathRule.ALWAYS );
            }
        }
    }
}