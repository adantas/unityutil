﻿using System.IO;
using UnityEngine;
using UnityEngine.Assertions.Must;


namespace UnityUtil
{
    public static class ImageUtil
    {
        /// <summary>
        /// Accepts a 2DTexture. And 2 Materials that will contain the 2 halves of the image when the method returns.
        /// The first output material is top half of the image, the second is be the bottom half of the image.
        /// </summary>
        /// <param name="texture">Texture to be split.</param>
        /// <param name="topMaterial"> When this methid returns, contains the material relating to the upper part of the image </param>
        /// <param name="bottomMaterial"> When this method returns, contains the material relating to the lower part of the image </param>
        /// <param name="shader"> Shader for the materials</param>
        public static void SplitImage(Texture2D texture, out Material topMaterial, out Material bottomMaterial, Shader shader)
        {
            shader.MustNotBeNull();

            Texture2D bottomTexture = new Texture2D(texture.width, Mathf.FloorToInt(texture.height / 2f));
            bottomTexture.SetPixels(texture.GetPixels(0, 0, bottomTexture.width, bottomTexture.height));
            bottomTexture.Apply();
            bottomMaterial = new Material(shader) { mainTexture = bottomTexture };

            Texture2D topTexture = new Texture2D(texture.width, Mathf.CeilToInt(texture.height / 2f));
            topTexture.SetPixels(texture.GetPixels(0, bottomTexture.height, texture.width, topTexture.height));
            topTexture.Apply();
            topMaterial = new Material(shader) { mainTexture = topTexture };
        }

        /// <summary>
        /// Accepts as input a string filename with no path. And 2 Materials that will contain the 2 halves of the image when the method returns.
        /// The filename is the name of a image file in PNG or JPG format. 
        /// The first output material is top half of the image, the second is be the bottom half of the image.
        /// </summary>
        /// <param name="filename"> Name of the image file in the root of any folder named "Resources" anywhere in the Assets folder </param>
        /// <param name="topMaterial"> When this methid returns, contains the material relating to the upper part of the image </param>
        /// <param name="bottomMaterial"> When this method returns, contains the material relating to the lower part of the image </param>
        /// <param name="shader"> Shader for the materials</param>
        public static void SplitImage(string filename, out Material topMaterial, out Material bottomMaterial, Shader shader)
        {
            Texture2D originalTexture = GetTextureFromResources(filename);
            SplitImage(originalTexture, out topMaterial, out bottomMaterial, shader);
        }


        /// <summary>
        /// Returns a Texture2D from a named file in the root of any folder named "Resources" anywhere in the Assets folder.
        /// Throws FileNotFoundException exception if the file isnt found.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static Texture2D GetTextureFromResources( string filename )
        {
            // Check if file Exists
            Texture2D texture = Resources.Load( filename ) as Texture2D;
            if ( texture == null )
            {
                throw new FileNotFoundException( string.Format(
                    "Problem loading {0}. Make sure to omit file extension when calling method and that file with either .png or .jpg extension is in a valid Resources folder.",
                    filename ) );
            }

            // Make sure texture is readable
            try
            {
                texture.GetPixel( 0, 0 ); // This is only way to test if texture is readable
            }
            catch ( UnityException e )
            {
                if ( e.Message == string.Format(
                    "Texture '{0}' is not readable, the texture memory can not be accessed from scripts. You can make the texture readable in the Texture Import Settings.",
                    filename ) )
                {
                    throw new UnityException( string.Format( "Texture '{0}' is not readable.",
                        filename ) );
                }
                throw;
            }

            return texture;
        }
    }
}