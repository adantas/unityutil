﻿using UnityEngine;
using UnityEngine.Assertions;

namespace UnityUtil
{
    public static class MeshUtil
    {
        /// <summary>
        /// Takes the size of each plane a pivot and returns a Mesh 
        /// that should to be added to a MeshFilter on a GameObject that also 
        /// has a Mesh Renderer.
        /// </summary>
        /// <param name="size">Size of the plane</param>
        /// <param name="pivot">Determines where the pivot of the mesh will be. Vector2.zero places the mesh in the center of the GameObject.</param>
        /// <param name="name">Name for the mesh</param>
        /// <returns></returns>
        public static Mesh CreatePlaneMesh( Vector2 size, Vector2 pivot, string name = "simpleplane" )
        {
            Assert.IsTrue(size.x > 0);
            Assert.IsTrue(size.y > 0);
            pivot = pivot.normalized;
            
            var x = size.x/2;
            var y = size.y/2;

            Vector3 offset = new Vector3( pivot.x*x, pivot.y*y, 0 );

            Mesh mesh = new Mesh
            {
                name = name,
                vertices = new Vector3[]
                {
                    new Vector3( -x, -y, 0 ) + offset,
                    new Vector3( x, -y , 0 ) + offset,
                    new Vector3( x, y, 0 ) + offset,
                    new Vector3( -x, y, 0 ) + offset
                },
                uv = new Vector2[]
                {
                    new Vector2( 0, -1 ),
                    new Vector2( -1, -1 ),
                    new Vector2( -1, 0 ),
                    new Vector2( 0, 0 )
                },
                triangles = new int[] {0, 1, 2, 0, 2, 3}
            };


            mesh.RecalculateNormals( );
            return mesh;
        }
    }
}