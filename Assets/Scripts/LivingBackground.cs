﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Eppy;

namespace UnityUtil
{
    public class LivingBackground : MonoBehaviour
    {
        [SerializeField] private List<Texture2D> layerTextures;
        private List<GameObject> layerGameObjects;

        private const string ShaderName = "Unlit/Desaturated Transparent";
        private Shader shader;
        private string folderName;

        private const float LayerGap = .01f; // Z distance between layers
        private const float PixelsPerUnit = 100;

        /// <summary>
        /// Read all the available layers into memory (stores them disabled in scene), confirming that they are the same size.
        /// </summary>
        /// <param name="folderName">String representing the folder where the resource is located. </param>
        public void LoadLayers( string folderName )
        {
            this.folderName = folderName;
            // Load Textures
            layerTextures = ( ( Texture2D[] ) Resources.LoadAll<Texture2D>( folderName + "/" ) ).ToList( );
            if ( layerTextures.Count == 0 )
            {
                throw new FileNotFoundException( "The specified folder \"" + folderName + "\" couldn't be found or does not contain images." );
            }

            Vector2 imageSize = new Vector2( layerTextures[0].width, layerTextures[0].height );
            if ( layerTextures.Select( texture => new Vector2( texture.width, texture.height ) ).Any( layerSize => imageSize != layerSize ) )
            {
                throw new InvalidOperationException( string.Format( "The specified folder '{0}' contains layers of different sizes, please make sure all the images in a layered image folder are untrimmed.", folderName ) );
            }

            LoadShader( );
            CreateLayerRenderers( );
        }

        private void LoadShader( )
        {
            shader = Shader.Find( ShaderName ); // Shaders are only included in builds if they are assigned to a material that is used in any scene or if the shader is placed in a "Resources" folder.
            if ( shader == null )
            {
                throw new FileNotFoundException(
                    string.Format( "Problem loading shader {0}. Make sure to place that shader in a Resources folder.",
                        ShaderName ) );
            }
        }

        /// <summary>
        /// Create the GameObjects that will hold the renderers. Positions them.
        /// Calls CreateLayerPlanes to create the plane meshes - top and bottom.
        /// Calls SplitImage to populate the renderers.
        /// 
        /// </summary>
        private void CreateLayerRenderers( )
        {
            layerGameObjects = new List<GameObject>( );
            foreach ( var layer in layerTextures )
            {
                // Create GameObject to store renderers with planes
                GameObject layerGameObject = new GameObject( layer.name );
                layerGameObject.transform.parent = transform;
                layerGameObject.transform.localPosition = Vector3.zero;

                // Determine size of planes
                var width = layer.width/PixelsPerUnit;
                var height = layer.height/PixelsPerUnit/2; // Half height because the image will be clipped

                // Create the plane meshes
                var planeGameObjects = CreateLayerPlanes( layerGameObject, width, height );

                // Get the Materials
                Material topMaterial = planeGameObjects.Item1.GetComponent<Renderer>( ).sharedMaterial;
                Material bottomMaterial = planeGameObjects.Item2.GetComponent<Renderer>( ).sharedMaterial;
                ImageUtil.SplitImage( layer, out topMaterial, out bottomMaterial, shader );

                // Assign them to the planes
                planeGameObjects.Item1.GetComponent<Renderer>( ).sharedMaterial = topMaterial;
                planeGameObjects.Item2.GetComponent<Renderer>( ).sharedMaterial = bottomMaterial;

                // position the layer in the Z according to it's number
                layerGameObject.transform.localPosition = Vector3.forward*LayerGap*layerGameObjects.Count; // forward is in fact further from camera. positive Z
                layerGameObjects.Add( layerGameObject );
                layerGameObject.SetActive( false ); // By default no layers are actually displayed.
            }
        }

        /// <summary>
        /// Create a GameObject for each plane - up plane and bottom plane. Add the renderers to it and create mesh according to pivot of plane.
        /// Top plane has pivot Vector2.down, while Bottom has Vector2.down.
        /// This way when both planes are in a GameObject in the same position both images coincide.
        /// </summary>
        /// <param name="parent">Parent of both layer planes.</param>
        /// <param name="width">Width of each plane</param>
        /// <param name="height">Height of each planes</param>
        private static Tuple<GameObject, GameObject> CreateLayerPlanes( GameObject parent, float width, float height )
        {
            string[] planeNames = new string[2] {"top", "bottom"};
            Vector2[] pivots = new Vector2[2] {Vector2.up, Vector2.down};

            GameObject[] planeGameObjects = new GameObject[2];
            for ( int i = 0; i != 2; i++ )
            {
                GameObject plane = new GameObject( planeNames[i] );
                var meshFilter = plane.AddComponent<MeshFilter>( );
                meshFilter.sharedMesh = MeshUtil.CreatePlaneMesh( new Vector2( width, height ), pivots[i] ); // Custom mesh is better for performance and easier to map uv
                MeshRenderer meshRenderer = plane.AddComponent( typeof ( MeshRenderer ) ) as MeshRenderer;
                meshRenderer.material.shader = Shader.Find( ShaderName );

                plane.transform.parent = parent.transform;
                plane.transform.localPosition = Vector3.zero;
                plane.transform.localRotation = Quaternion.Euler( 0, 180, 0 );
                planeGameObjects[i] = plane;
            }
            return new Tuple<GameObject, GameObject>( planeGameObjects[0], planeGameObjects[1] );
        }

        /// <summary>
        /// Cause the specified layer number to be shown.
        /// </summary>
        /// <param name="layerNumber">Number of the layer to be shown. 0 is the one above all others.</param>
        /// <param name="howMuchColor">Determines the amount of color in the image.</param>
        public void ShowLayer( int layerNumber, float howMuchColor )
        {
            // Index out of bounds check
            int layerCount = layerGameObjects.Count - 1;
            if ( layerNumber < 0 || layerNumber > layerCount )
            {
                throw new IndexOutOfRangeException( string.Format( "Array index out of range. Accessing non-existent layer. Layer images in folder '{0}' go from 0 to {1}", folderName, layerCount ) );
            }

            // Wierd color check
            if ( howMuchColor < 0 || howMuchColor > 1.0f )
            {
                Debug.LogWarning( string.Format( "You are showing layer {0} of image {1} with a wierd ammount of saturation", layerNumber, folderName ) );
            }

            // Show the layer and set the amount of color
            layerGameObjects[layerNumber].SetActive( true );
            Renderer[] renders = layerGameObjects[layerNumber].GetComponentsInChildren<Renderer>( );
            foreach ( Renderer render in renders )
            {
                render.sharedMaterial.SetFloat( "_HowMuchColor", howMuchColor );
            }
        }

        public List<GameObject> GetLayerGameObjects( )
        {
            if ( layerGameObjects == null )
            {
                throw new NullReferenceException( "You're trying to access layers before calling LoadLayers method to load them." );
            }
            return layerGameObjects;
        }
    }
}