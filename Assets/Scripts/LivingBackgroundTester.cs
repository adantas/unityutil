﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UnityUtil
{
    public class LivingBackgroundTester : MonoBehaviour
    {
        [SerializeField] private string folderName = "starrynight";

        private const float TimeIncrements = .2f;

        public void Start( )
        {
            LivingBackground livingBackground = gameObject.AddComponent<LivingBackground>( );

            livingBackground.LoadLayers( folderName );

            // Showcase, iterate through all the layers of the living background and show every layer on tick tick
            for ( int i = 0; i != livingBackground.GetLayerGameObjects( ).Count; i++ )
            {
                float randomSaturation = Random.Range( 0, 1.0f );
                var layerNumber = i;
                Action action = ( ) => livingBackground.ShowLayer( layerNumber, randomSaturation );
                StartCoroutine( CoroutineUtil.WaitAndDo( action, TimeIncrements*( layerNumber + 1 ) ) );
            }
        }
    }
}