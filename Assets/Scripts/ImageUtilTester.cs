﻿using System.IO;
using UnityEngine;

namespace UnityUtil
{
    public class ImageUtilTester : MonoBehaviour
    {
        public string filename;

        public void Start( )
        {
            //Creates 2 Planes to preview these materials
            GameObject topPlane = GameObject.CreatePrimitive( PrimitiveType.Plane );
            topPlane.name = "top";
            topPlane.transform.parent = Camera.main.transform;
            topPlane.transform.localPosition = Vector3.forward*20 + Vector3.up*5.5f;
            topPlane.transform.localRotation = Quaternion.Euler( 90, 180, 0 );

            GameObject bottomPlane = GameObject.CreatePrimitive( PrimitiveType.Plane );
            bottomPlane.name = "bottom";
            bottomPlane.transform.parent = Camera.main.transform;
            bottomPlane.transform.localPosition = Vector3.forward*20 + Vector3.down*5.5f;
            bottomPlane.transform.localRotation = Quaternion.Euler( 90, 180, 0 );

            Material topMaterial = topPlane.GetComponent<Renderer>( ).sharedMaterial;
            Material bottomMaterial = bottomPlane.GetComponent<Renderer>( ).sharedMaterial;

            const string shaderName = "UI/Default";
            Shader shader = Shader.Find( shaderName ); // Shader are only included in builds if they are assigned to a material that is used in any scene or if the shader is placed in a "Resources" folder.

            if ( shader == null )
            {
                throw new FileNotFoundException(
                    string.Format( "Problem loading shader {0}. Make sure to place that shader in a Resources folder.",
                        shaderName ) );
            }

            // Get the Materials
            ImageUtil.SplitImage( filename, out topMaterial, out bottomMaterial, shader );

            // Assign them to the planes
            topPlane.GetComponent<Renderer>( ).sharedMaterial = topMaterial;
            bottomPlane.GetComponent<Renderer>( ).sharedMaterial = bottomMaterial;
        }
    }
}